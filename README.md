Primeiro crie o fluxo UDP no VLC: https://www.youtube.com/watch?v=jCPJYJEEwyU

Usando IP local e porta 5000.

O vídeo você encontra em: [https://bitbucket.org/AdonisBruno/produtor-consumidor_/src/acd9a0e42eda1adaee4d96208a70123ea212bb2d/Video/?at=master](Link URL)

Abra mais 5 VLC, cada um em uma porta, 5010, 5020, 5030, 5040, 5050.

Execute o código... 

O que deve acontecer é o vídeo ser executado simultaneamente nos 5 VLC.