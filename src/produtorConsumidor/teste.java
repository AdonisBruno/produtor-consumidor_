package produtorConsumidor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class teste {

	static int sizeBuffer = 500;
	static List<byte[]> buffCompar = new ArrayList<byte[]>(sizeBuffer);

	static Semaphore empty = new Semaphore(sizeBuffer);//new Semaphore(sizeBuffer);
	static Semaphore full = new Semaphore(0);

	public static void main(String[] args) {

		// escuto na porta 4000
		//................Produtor(IP_ORIGEM, PORTA_ORIGEM, IP_DESTINO, PORTA_DESTINO_VLC, empty, full);
		//Produtor(String IP, int PORTA, int idProdutor, List<byte[]> buffer, Semaphore empty, Semaphore full)
//		
		Produtor p1 = new Produtor(5000,buffCompar, empty, full);
//		
		Consumidor c1 = new Consumidor("127.0.0.1", 5010, 1, buffCompar, empty, full);
		Consumidor c2 = new Consumidor("127.0.0.1", 5020, 2, buffCompar, empty, full);
		Consumidor c3 = new Consumidor("127.0.0.1", 5030, 3, buffCompar, empty, full);
		Consumidor c4 = new Consumidor("127.0.0.1", 5040, 4, buffCompar, empty, full);
		Consumidor c5 = new Consumidor("127.0.0.1", 5050, 5, buffCompar, empty, full);

		p1.start();
		c1.start();
		c2.start();
		c3.start();
		c4.start();
		c5.start();

	}

}
