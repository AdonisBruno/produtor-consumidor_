package produtorConsumidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Produtor extends Thread {

	// private int idProdutor;
	private List<byte[]> buffer;
	// private int[] value = new int[teste.sizeBuffer];// para debug
	// private static long temp = 1; // para debug
	Semaphore empty;// = new Semaphore(1);
	Semaphore full;// = new Semaphore(0);
	static int rear = 0;
	// static Semaphore mutexD = new Semaphore(1); // para exclusao mutua

	static int SIZE_BUFFER = teste.sizeBuffer;

	byte data[] = new byte[1316];
	private DatagramPacket receivePacket;
	private DatagramSocket socket;

	// private String IP;
	// private int PORTA;

	// private DatagramPacket sendPacket;
	// private DatagramSocket socketSend;
	// private String IPD; //IP destino
	// private int PORTAD; //Porta destino

	// public Produtor(String IP, int PORTA, int idProdutor, List<byte[]>
	// buffer, Semaphore empty, Semaphore full) {

	// public Produtor(String IP, int PORTA, String IPD, int PORTAD, Semaphore
	// empty, Semaphore full) {
	public Produtor(int PORTA, List<byte[]> buffer, Semaphore empty, Semaphore full) {
		// this.IP = IP;
		// this.PORTA = PORTA;
		try {
			this.socket = new DatagramSocket(PORTA);
			// this.socketSend = new DatagramSocket(PORTAD+1);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		// this.idProdutor = idProdutor;
		this.buffer = buffer;
		this.empty = empty;
		this.full = full;
		// this.IPD = IPD;
		// this.PORTAD = PORTAD;
	}

	public void run() {

		while (true) {
			try {
				/* Produz o dado a ser depositado no buffer */

				// recebe pacotes da porta 5000
				this.receivePacket = new DatagramPacket(data, 1316);
				socket.receive(receivePacket);

				// System.err.println("tam = " + receivePacket.getLength());
				// porta a ser enviada (VLC)
				// sendPacket = new DatagramPacket(receivePacket.getData(),
				// receivePacket.getLength(),
				// InetAddress.getByName(IPD), PORTAD);
				// socket.send(sendPacket);

				empty.acquire(); // protocolo de entrada consumidores)

				buffer.add(rear, receivePacket.getData());
				rear = (rear + 1) % teste.sizeBuffer;

				full.release();
				
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			// System.out.println("# " + idProdutor +" # " +
			// Arrays.toString(value));
		}
	}

}
