package produtorConsumidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Consumidor extends Thread {

	private int idConsumidor;
	private List<byte[]> buffer;
	Semaphore empty;				// = new Semaphore(1);
	Semaphore full;					// = new Semaphore(0);
	static int front = 0;
	static Semaphore mutexF = new Semaphore(1); // para exclusao mutua
	

	static byte data[] = new byte[1316];
	private DatagramPacket sendPacket;
	private DatagramSocket[] socket = new DatagramSocket[10]; // maximo 10 threads

	private String IP;
	private int PORTA;
	
	static int qtd_consumidores = 0;
	static int count_consumidores = 0;


	public Consumidor(String IP, int PORTA, int idConsumidor, List<byte[]> buffer, Semaphore empty, Semaphore full) {
		this.IP = IP;
		this.PORTA = PORTA;
		this.idConsumidor = idConsumidor;
		this.buffer = buffer;
		this.empty = empty;
		this.full = full;
		try {
			this.socket[idConsumidor] = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		qtd_consumidores++;
	}

	public void run(){
		while(true){
			try {
				mutexF.acquire(); // protocolo de entrada (exclusão mutua)
				full.acquire(); // protocolo de entrada (cesso ao buffer limitado)
								
				data = buffer.get(front);
				sendPacket = new DatagramPacket(data, data.length, InetAddress.getByName(IP), PORTA);
				socket[idConsumidor].send(sendPacket);
				count_consumidores++;
				
				if(qtd_consumidores == count_consumidores)
					front = (front+1) % teste.sizeBuffer;

				empty.release(); //  protocolo de saida (cesso ao buffer limitado)
				mutexF.release(); // protocolo de saida (exclusao mutua)
			} catch (InterruptedException | IOException e) {
				e.printStackTrace();
			}
		}
	}

}
